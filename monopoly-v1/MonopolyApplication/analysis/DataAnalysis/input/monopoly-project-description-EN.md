# Title page
 * assignment : Data Analysis
 * game : Monopoly Terminal Game
 * group : Lecturer
# Project Description
## GAME DESCRIPTION:
Monopoly is a board game in which several players try to gain financial dominance (monopoly) in a real estate market. 
In turn, a player will move his pawn forward on the board, performing a series of actions where she lands. 
For example, she can purchase properties and build a number of houses on them. Whenever other players then land on such a property they will have to pay the owner a rent. 
You win the game when all other players are bankrupt. 

Translated copy of https://allespelregels.nl/bordspellen/monopoly-spelregels/ follows below

### Monopoly game RULES
The aim of the game is the same for every player: to obtain a monopoly. You do this by buying streets. You get a certain amount of money ("rent") when another player lands on your street. If you build houses or hotels on your streets you receive even more rent. If you have many streets, houses and hotels all the money in the game flows to you. The other players then go bankrupt, and you win the game!

#### RULE 1. Starting a game of monopoly
   To determine who gets to start, each player rolls the two dice. The player who rolls the highest number gets to start. If players roll the same number, those players roll again. Then the order goes clockwise.

#### RULE 2. Playing a turn
   To play a turn you roll the two dice. The number you roll is the number of steps you may take.

If you land on a property (a street, stations or utility) that has not yet been sold, you can buy it.

So if you land on the "go to jail" square, you must go to jail. This can also happen if you draw a General Fund or Chance card that says you must go to jail, or if you roll double three times in one turn. If you have to go to jail you do not receive a salary.

To get out of jail you can pay, say, €50 and continue playing. Also, you can buy the card "leave jail without paying" from another player or use it if you have it yourself. Finally, you can wait three turns and try to throw doubles during those turns. After three turns in jail (without throwing doubles) you are obliged to pay €50 and leave jail. If only it were that easy in real life!

If you land on "visiting prison only" you don't have to do anything. The same goes for "free parking." If you pass by "salary" you get €200. If you land on "paying taxes" you have to, you guessed it, pay taxes!

When you land on property that has already been sold, you have to pay rent. How much rent you have to pay is listed on the property deed. The rent gets higher if the player owns more houses, hotels, stations or utilities. This is also indicated on the property deed.

#### RULE 3. Building houses and hotels
   You may only build houses in a street if you own all streets can a city (color). The price of a house is also shown on the title deed. You may only build a second house on a street if you have one house on each street. So you must build gradually. You may not build more in a town if any street in that town is mortgaged.

## PROGRAM REQUIREMENTS
The final game will have to take into account the following requirements:

 * You ensure that the game can be played by 2 players. Each player enters their name at the start of a new game. The computer gives the first player a white pawn, this player will also start the game. The other player is given the black pawn.
 * Players have up to 24h thinking time after each move of the previous player to execute their move, if this fails then the other player automatically becomes the winner.
 * After each game the outcome is automatically saved, along with the names of the players, a DateTime stamp when the game was played and the final winner(or draw). It also records how long this game lasted, how many rounds were played, and how much money each player had at the end.
 * This list of games played can be accessed from the main menu.
 * The game rules are implemented as described above. You do not have to take variants into account yet (see extensions to exceed expectations in the project)
 * In the first phase you create a console variant, here it is possible to move a piece by rolling the tiles.

## EXTENSIONS
Once you have programmed a working game with a nice graphical interface, you might want to work out one of these extensions:

 * Along with a table to consult all past games, it is now also possible to view a highscores table showing how many times each player has already won, as well as his ELO rating. See [here](https://nl.wikipedia.org/wiki/Elo-rating)  to understand how to calculate this rating.
 * A game can be saved at any time and resumed later.
 * Provide a single player mode where games can be played against the computer. This need not be a complex AI, but rather an algorithm that takes into account the rules of the game and possibly applies some strategies.