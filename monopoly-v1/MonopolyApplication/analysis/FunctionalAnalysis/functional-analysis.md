# Title page

* assignment : Functional Analysis
* game : Monopoly Terminal Game
* group : Lecturer

# User Stories
## Iteration 1 - start filling the user story template by brainstorming
Just write about any feature that you can think of, and afterwards evaluate using the following criteria 


Below is a first version of the user stories

| EPIC/Use Case       | As an [Actor/User] | I want to be able to [do a thing]                                                | so that I can [achieve a goal]                                   |
|:--------------------|:-------------------|:---------------------------------------------------------------------------------|:-----------------------------------------------------------------|
| Game Management     | Application        | Show the game rules                                                              | understand how to play the game                                  |
| Game Management     | Application        | Show the game rules                                                              | understand how to play the game                                  |
| Game Management     | Application        | Show information on how to play the ASCII version of the game.                   | The player knows how the game navigation works.                  |
| Game Management     | Application        | Create Player objects.                                                           | Use these objects for player management.                         |
| Game Management     | Application        | Ask a player for their name.                                                     | Playername can be added to a player.                             |
| Game Management     | Application        | Ask a player for their pawn.                                                     | Playerpawn can be added to a player.                             |
| Game Management     | Application        | Make it possible to add a player.                                                | Players can be added.                                            |
| Game Management     | Application        | Make an extra invisible player who will manage the bank.                         | Bank will be managed.                                            |
| Game Management     | Application        | Make a Dices object                                                              | Players will use this to throw the dices.                        |
| Game Management     | Application        | Let the Dices object roll.                                                       | Players can use the Dices.                                       |
| Game Management     | Application        | Show the bankruptcy rules.                                                       | The player knows the bankruptcy rules.                           |
| Game Management     | Application        | Show the current situation at a given time in the game.                          | The players know the situation.                                  |
| Game Management     | Application        | Register when the game has been played.                                          | The application knows when to stop.                              |
| Position Management | Application        | Define Position object attributes for all types (properties, bank, chance, etc.) | Position attributes are available to fill the board.             |
| Position Management | Application        | For each position, make a Position object with the correct attributes.           | Positions are available to fill the board.                       |
| Position Management | Application        | Ask each buyable position if it already has been bought.                         | Apply the right action when a player hits the position.          |
| Position Management | Application        | Ask each buyable position for its price.                                         | Apply the actions when a player hits the position.               |
| Position Management | Application        | Ask each position for legal actions.                                             | Apply the actions when a player hits the position.               |
| Position Management | Application        | Ask each position for its current rent.                                          | Apply the actions when a player hits the position.               |
| Position Management | Application        | Define attributes for the 4 corner positions.                                    | Apply the right action when a player hits the corner position.   |
| Board Management    | Application        | Use the Positions to fill the board.                                             | Have a correct board to play with.                               |
| Board Management    | Application        | Create an ASCII playing board based on the standard Monopoly board.              | Update and show it during the game.                              |
| Board Management    | Application        | Make 2 to 10 players visible on the start position on the board.                 | Use this as the start situation.                                 |
| Board Management    | Application        | Show the current amount of tax money and jail money after each turn.             | All players know the sum they can win.                           |
| Player Management   | Player             | Enter my name.                                                                   | The player name has been registered.                             |
| Player Management   | Player             | Enter my pawn.                                                                   | The player pawn has been registered.                             |
| Player Management   | Player             | Buy a property.                                                                  | I own the property.                                              |
| Player Management   | Player             | Buy a railway station.                                                           | I own the railway station.                                       |
| Player Management   | Player             | Receive rent on my properties.                                                   | Calculate my current currency position.                          |
| Player Management   | Player             | Ask the bank for a loan.                                                         | I can continue buying properties.                                |
| Player Management   | Player             | If I am in jail get out of it.                                                   | To continue playing.                                             | 
| Player Management   | Player             | When I reach the Go position I want to receive double fee.                       | To follow the rule.                                              |


1. Make sure to **NOT** include words that refer to a user interface like  
* click on the button
* go to the screen
* enter a command to do ...
* go to home page
* see the menu
* click on the board
* drag the pawn to a new position
* ...

1. Do **NOT** use actors that refer to the application that we want to implement, eg :
* System
* Computer
* Application
* Game
* ...   

1. Make sure that user stories are **NOT too large/vague**, eg. 
   * As a player i can play by the rules
   * As a user i can see the board 
   * As a player i can roll, move, pay _etc..._ (do no use ... or etc.)  
1. Make sure that user stories are **NOT too small/specific**, eg.  
a. As a player I can see the current time in the top-left position 
b. As a user I can see the name of a property 
c. As a user I can see the color of a property between brackets next to the name   
d. As a user I can see the value of a property under the name preceding by "value:"     
e. As a user I can see the rent of a property  
f. Steps b. to e. can be replaced by : As a user I can see all relevant info of a property (name,value,color,rent)


As we can see - our first iteration is not allows adhering to these anti-patterns. 

## Iteration 2 - Reorganize using a more structured approach
### Actor(s)?
There seems to be just 1 actor: A **player** during the game. 
We also introduce an extra actor which helps us to launch, setup and navigate the application : the **host**

By introducing this 2nd actor we hope to identify user stories that are not linked to the game itself, but to the full application

* player : performs actions in each turn/round of the game 
* host :  launches & navigates the application, starts a new game and ends the game.

### Components?
In this step we try to break the application into smaller components. 
That will make it easier to create an exhaustive list of user stories.
These components could be based on the different phases of the application
(start, middle, end) - or based on the use cases of a Use Case diagram,
These components are **NOT** screens  

Our application consists of the following components
* The application allows to launch multiple games
  * It consists of management of the board
  * It consists of management of the movements
  * It consists of all actions that can be performed by a player
  * It consists of the setup of the game/board
* The application allows handling top scores on a leaderboard
* The application provides screens for the rules and a help page which explains the 'keys/command' to type

### Use Case diagram
``` plantuml
@startuml
left to right direction
'top to bottom direction

note right of Host : The actor that operates\n the system when **not** \nplaying the game.
note right of Player : An actor that operates\n the system **while** \nplaying the game.

rectangle "Monopoly Application" {
Player -> (Perform a turn)
(Perform a turn) ..> (Perform a legal action)
Player --> (See all relevant information)

Host -> (Launch the application)
Host -> (Start a new game)
Host -> (Navigate the application)
}

Host -r-|> Player
@enduml
``` 

### Second version of User stories
We will rewrite the first version of the user stories 
* Use actors and use cases from the use case diagram
* Don't use the anti-patterns which are listed above.


| EPIC/Use Case      | As an [Actor/User] | I want to be able to [do a thing]                                                                                                                       | so that I can [achieve a goal]                                                                                              |
|:-------------------|:-------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------|
| Setup the Game     | Host               | Read the game rules                                                                                                                                     | understand how to play the game                                                                                             |
| Setup the Game     | Host               | Read information on how to play the ASCII version of the game. ie. the “commands/keys” help page                                                        | know how the game navigation works.                                                                                         |
| Setup the Game     | Host               | define the number of players                                                                                                                            | play the game with multiple people                                                                                          |
| Setup the Game     | Host               | Enter a player's name.                                                                                                                                  | know when it is my turn                                                                                                     |
| Setup the Game     | Host               | Enter the players choice for pawn type or color                                                                                                         | express myself and easily find back my position on the board                                                                |
| Setup the Game     | Host               | see who has won in previous game                                                                                                                        | reflect                                                                                                                     |
| Setup the Game     | Host               | can choose to launch the game                                                                                                                           | start playing with my friends                                                                                               |
| Setup the Game     | Host               | provide starting money to each player                                                                                                                   | ensure that players have enough cash to play the game                                                                       |
| Setup the Game     | Host               | Provide two  6-sided dice                                                                                                                               | ensure that players can generate a random number at the start of their turn                                                 |
| Setup the Game     | Host               | provide a visual representation of the game board, all it’s 40 different squares (in the details of the story there should be a list of all 40 Squares) | ensure that players always have a map of what are the potential landing positions during the game                           |
| Setup the Game     | Host               | place all player’s pawn at “Go” position                                                                                                                | ensure that players have an equal starting position.                                                                        |
| Setup the Game     | Host               | pick a random order for playing order                                                                                                                   | ensure that players have an equal chance of being the first, second, third  player in a round                               |
| Status information | Player             | always (during my turn, and during other players turn) know the position of my and other players pawn                                                   | change my tactical actions (buying/selling/building..) based on my position in relation to others                           |
| Status information | Player             | always (during my turn, and during other players turn) know the amount of money that I and others have                                                  | change my tactical actions (buying/selling/building..) based on my cash flow                                                |
| Status information | Player             | always (during my turn, and during other players turn) know the properties that each player has purchased                                               | change my tactical actions (buying/selling/building..) based on the risk of having to pay rent, and allowing to negotiate   |
| Status information | Player             | always (during my turn, and during other players turn) know  how much rent is to paid when players would land on each of the properties                 | evaluate the risk before rolling or adapt my strategy                                                                       |
| Status information | Player             | decide to know which special cards are in my possession without showing other players                                                                   | adapt my tactical actions during my turn while hiding that information from others                                          |
| Status information | Player             | see how long this turn has been ongoing                                                                                                                 | decide if I need to play faster                                                                                             |
| Status information | Player             | See how long this game has been ongoing                                                                                                                 | Estimate if I can finish the game before supper                                                                             |
| Status information | Player             | See how which round we are playing                                                                                                                      | Estimate in which phase of the game we are                                                                                  |
| Status information | Player             | See if any pawn is landed within JAIL or just visiting the JAIL                                                                                         | see if some player is stuck  in jail or not                                                                                 |
| Status information | Player             | Know for each property who owns it (or if is still unsold)                                                                                              | understand what is still for sale and where rent is to be paid                                                              |
| Status information | Player             | Know if I lost  or won the game                                                                                                                         | either celebrate or be sad                                                                                                  |
| Play a Turn        | Player             | know when it’s my turn to play during each round                                                                                                        | execute the actions needed during the game and don’t keep anyone waiting                                                    |
| Play a Turn        | Player             | roll the dice                                                                                                                                           | define the new position on which I will land                                                                                |
| Play a Turn        | Player             | roll two times the same number of dice (double)                                                                                                         | roll again at the end of this turn                                                                                          |
| Play a Turn        | Player             | roll three consecutive doubles and move to jail                                                                                                         | move my position directly to “Jail”                                                                                         |
| Play a Turn        | Player             | move my pawn forward equal to the total value of both dice I rolled                                                                                     | advance on the board and perform actions based on the new landing position                                                  |
| Play a Turn        | Player             | buy a property when I land on an unsold property                                                                                                        | build houses on it in a later turn                                                                                          |
| Play a Turn        | Player             | pay rent to another player when I land on a sold property also taking into account the houses built on the property                                     |
| Play a Turn        | Player             | buy a railway station when I land on an unsold railway station                                                                                          | try to collect them all                                                                                                     |
| Play a Turn        | Player             | ask the bank for a mortgage.                                                                                                                            | I can continue buying properties or pay of rent                                                                             |`
| Play a Turn        | Player             | get out of jail by paying money                                                                                                                         | I can continue playing                                                                                                      |
| Play a Turn        | Player             | stay in the jail for a max of three rounds                                                                                                              | take some rest                                                                                                              |
| Play a Turn        | Player             | get out of jail by using a “Get Out Jail Free” – card which is in my possession                                                                         | I can continue playing                                                                                                      |
| Play a Turn        | Player             | get out of jail by  rolling double                                                                                                                      | I can continue playing                                                                                                      |
| Play a Turn        | Player             | receive Start money when passing the “Go” square                                                                                                        | I can build up my wealth                                                                                                    |
| Play a Turn        | Player             | see which potential actions I can perform before rolling the dice                                                                                       | prepare myself before rolling                                                                                               |
| Play a Turn        | Player             | see which potential actions I can perform after landing on a new position                                                                               | know what are the legal moves for each position                                                                             |
| Play a Turn        | Player             | --- all other rules of the monopoly game are adhered to --                                                                                              | - finish all user stories -                                                                                                 |
| End Game           | Player             | have an overview of each player’s total wealth at the end of the game                                                                                   | How did we score on the game                                                                                                |
| End Game           | Player             | have an clear understanding how many rounds have been played at the end of the game                                                                     | How did we score on the game                                                                                                |
| End Game           | Player             | the option to either quit the game or start a new game                                                                                                  | Start again                                                                                                                 |
| End Game           | Player             | when starting a new game I get the option to play with the same players                                                                                 | Start again                                                                                                                 |
| End Game           | Player             | keep track of when this game was played and how long it took in minutes                                                                                 | reflect on the game                                                                                                         |

    

