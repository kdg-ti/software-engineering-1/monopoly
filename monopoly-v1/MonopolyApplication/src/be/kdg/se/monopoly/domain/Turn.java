package be.kdg.se.monopoly.domain;

import java.time.Duration;
import java.time.LocalDateTime;

public class Turn {
    //primitive attributes
    int turnNumber;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;

    //derivative attributes
    private Duration getDuration() {
        return Duration.between(startDateTime, endDateTime);
    }
}
